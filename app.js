let outerBox = document.querySelector(".outer-box");

for (let num = 0; num < 64; num++) {
  let smallBox = document.createElement("div");
  smallBox.classList.add("inner-box");
  smallBox.id = num;
  smallBox.setAttribute("i", num % 8);
  smallBox.setAttribute("j", Math.floor(num / 8));

  if (Math.floor(num / 8) % 2 == 1) {
    if (num % 2 == 1) {
      smallBox.classList.add("black-background");
    }
  } else {
    if (num % 2 == 0) {
      smallBox.classList.add("black-background");
    }
  }
  outerBox.appendChild(smallBox);
}

let boxes = document.querySelectorAll(".inner-box");
boxes.forEach((box) => box.addEventListener("click", colorChange));

function colorChange() {
  let i = this.getAttribute("i");
  let j = this.getAttribute("j");

  boxes.forEach((box) => box.classList.remove("red-color"));

  let a = i;
  let b = j;
  while (a < 8 && b < 8) {
    boxes.forEach((box) => {
      if (box.getAttribute("i") == a && box.getAttribute("j") == b) {
        box.classList.add("red-color");
      }
    });
    a++;
    b++;
  }

  a = i;
  b = j;
  while (a >= 0 && b >= 0) {
    boxes.forEach((box) => {
      if (box.getAttribute("i") == a && box.getAttribute("j") == b) {
        box.classList.add("red-color");
      }
    });
    a--;
    b--;
  }

  a = i;
  b = j;
  while (a >= 0 && b < 8) {
    boxes.forEach((box) => {
      if (box.getAttribute("i") == a && box.getAttribute("j") == b) {
        box.classList.add("red-color");
      }
    });
    a--;
    b++;
  }

  a = i;
  b = j;
  while (a < 8 && b >= 0) {
    boxes.forEach((box) => {
      if (box.getAttribute("i") == a && box.getAttribute("j") == b) {
        box.classList.add("red-color");
      }
    });
    a++;
    b--;
  }
}
